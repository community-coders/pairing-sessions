# Community Coders Pairing Sessions

The goal of this repository is to have a place where we, the GitLab Community Coders,
can keep track of our pairing sessions and where we can schedule the next ones.

In order to achieve this goal, this project serves as an issue tracker.
Amongst other things, issues may correspond to:

- Keep track, plan or request pairing sessions, both regular and ad-hoc
- Making proposals in general
- Document the services, tools and processes used
- Discussing automations

## Our channels

- [Twitch](https://www.twitch.tv/gitlabcommunitycoders)
- [YouTube](https://www.youtube.com/channel/UCVxPq_cVcZ5-ga9OTHKmuOg)

## Tuesday sessions

Usually we have our pairing session every Tuesday at around 10:15 UTC
and we stream it [on our Twitch channel](#our-channels).

The sessions don't have any specific timing, we might need 20 minutes as well
as close to one hour, but we generally tend to not exceed the one hour mark.

> Keep in mind that these sessions are run by us, the community, so the schedule
may not get respected, e.g. we might miss a session if no one can make it :wink:.

After each session end, we upload the stream to our [YouTube channel](#our-channels).
In future we may occasionally also upload to the [GitLab Unfiltered YouTube channel](https://www.youtube.com/c/GitLabUnfiltered).

### How to suggest a topic for a session

For almost every session we try to have a topic, around which the session will
be focused on.

In order to decide what the topic for the next session will be, we use the issue
tracker on this repo.

Every Tuesday, a new issue for the next Tuesday's session will be automatically
generated and anyone is welcome to provide any suggestion for the topic in there.

## Ad-hoc sessions

Sometimes someone could have the need of some help, or even just want to share
some work they're doing, and might want to stream it so that everyone from the
Community can watch and, hopefully, learn something useful for the future.

To keep track, or even request, one of these sessions, anyone can open an issue
asking for it.

On the issue description, it would be good to:

- know what the topic of the session will be
- directly ping someone if their help is needed

## Grow the "admins" team

At the moment, there are three people in charge of this project and the accounts
we registered on Twitch and Restream. They are:

- Andrew Smith (`@espadav8`)
- Lee Tickett (`@leetickett`)
- Marco Zille (`@zillemarco`)

To be able to stream a session, at least one of them must be part of the session
and will be responsible for the streaming and any further upload to other platforms
(for example our YouTube channel).

Since this is a Community initiative, and we're all part of the Community, we are
open to grow this admins team so that more sessions will be possible.

This also means that we will share any information to be able to login to our accounts
and, since that's sensitive data, we prefer not to share them too soon: after some
sessions and when some mutual trust has grown, anyone who is interested can open
an issue in here asking to become part of the admins team.

## Setup for streaming

To do our streams we use the service provided by [Restream](https://restream.io/)
and as such we registered an account which is already set up to stream on
[our Twitch channel](#our-channels).

Once logged in, the host just needs to make sure to:

- update the stream title
- enable any partecipant/guest who might be in the waiting room
- start the stream and go live :rocket:
